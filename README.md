# grunt-atlassian-soy-compiler

Uses Atlassian's Soy compiler to compile Soy files to JS.

## Getting Started
This plugin requires Grunt `>=1.0.0`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```
$ npm install --save-dev @refinedwiki/grunt-atlassian-soy-compiler
```

## Usage

```js
require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks

grunt.initConfig({
    soy: {
        options: {
          propertiesFiles: [path.join('properties', 'strings.properties')]
        },
        default: {
          src: '.', // Must be the root in order for propertis to work
          dest: path.join('build', 'soy'),
          glob: path.join('public', 'templates', '*.soy'),
          translationTarget: true
        },
    }
});

grunt.registerTask('default', ['soy']);
```

### Options

#### propertiesFiles
Type: `Array`

A list of .properties files containing translations

### Task properties

#### skipTranslations
Type: `Boolean`
Default: `false`

If it's true the task uses the supplied `propertiesFiles` verbatim. Otherwise the task expects the target to be named after the language (For example de_DE) and uses that to search for the relevant .properties file. The target for the default language should be named default.

#### src
Type: `String`

This must be a directory that includes both the directory with properties as well as the directory with the Soy files
 
#### glob
Type: `String`

The glob used to find Soy files
