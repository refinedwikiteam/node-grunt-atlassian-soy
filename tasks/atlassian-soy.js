'use strict';
const Promise = require('bluebird');
const _ = require('lodash');
const soyCli = require('atlassian-soy-cli');

module.exports = function(grunt) {
  grunt.registerMultiTask('soy', 'Soy', function() {
    const done = this.async();
    const taskOptions = this.options();
    const propertiesFiles = taskOptions.propertiesFiles;
    if (!propertiesFiles) {
      grunt.fail.fatal('No properties file locations specified');
    }
    if (propertiesFiles.constructor !== Array) {
      grunt.fail.fatal('propertiesFiles must be an array');
    }
    for (let i = 0; i < propertiesFiles.length; i++) {
      if (propertiesFiles[i].indexOf('.properties') < 0) {
        propertiesFiles[i] = propertiesFiles[i] + '.properties';
      }
    }
    grunt.log.oklns('Compiling templates for ' + this.target);
    let i18n = '';
    // If there is a language set, search for strings in any properties file with that language
    if (this.target !== 'default' && !this.data.skipTranslations) {
      i18n = '**/*_' + this.target + '.properties';
    }
    else {
      // Otherwise join all the english properties files
      i18n = '{' + _.join(propertiesFiles, ',') + '}';
    }
    this.data.glob = '{' + this.data.glob + '}';
    soyCli({
      version: '3.4.2',
      basedir: this.data.src,
      type: 'js',
      outdir: this.data.dest,
      extraArgs: {
        i18n: i18n,
        extension: (this.target !== 'default' ? this.target + '.' : '') + 'soy.js'
      }
    }).compile(this.data.glob).
      then(() => {
        done();
      }).catch((err) => {
        grunt.fail.fatal(err);
      });
  });
};
